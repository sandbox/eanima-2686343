<?php

/**
 * @file
 * The Block Inject module functions.
 */

/**
 * Implements hook_boot().
 *
 * In order to make sure Block Inject gets loaded very early on in the
 * bootstrapping phase.
 */
function block_inject_adv_boot() {
// Empty on purpose.
}

/**
 * Implements hook_permission().
 */
function block_inject_adv_permission() {
  return array(
    'administer block inject' => array(
      'title' => t('Administer the Block Inject module'),
    ),
    'override default block injection' => array(
      'title' => t('Override default behaviour per node'),
    ),
  );
}

/**
 * Implements hook_menu().
 */
function block_inject_adv_menu() {
  $items['admin/structure/block-inject'] = array(
    'title' => 'Block Inject',
    'description' => 'Create block regions to inject into content',
    'page callback' => 'block_inject_adv_list',
    'access arguments' => array('administer block inject'),
    'type' => MENU_NORMAL_ITEM,
    'file' => 'block_inject_adv.admin.inc',
  );

  $items['admin/structure/block-inject/exceptions'] = array(
    'title' => 'Exceptions',
    'description' => 'Nodes excepted from being injected',
    'page callback' => 'block_inject_adv_exceptions_list',
    'access arguments' => array('administer block inject'),
    'type' => MENU_NORMAL_ITEM,
    'file' => 'block_inject_adv.admin.inc',
  );

  $items['admin/structure/block-inject/add'] = array(
    'title' => 'Add inject region',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('block_inject_adv_add_inject_form'),
    'access arguments' => array('administer block inject'),
    'type' => MENU_LOCAL_ACTION,
    'file' => 'block_inject_adv.admin.inc',
  );

  $items['admin/structure/block-inject/%/edit'] = array(
    'title' => 'Edit block inject region',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('block_inject_adv_add_inject_form', 3),
    'access arguments' => array('administer block inject'),
    'type' => MENU_CALLBACK,
    'file' => 'block_inject_adv.admin.inc',
  );

  $items['admin/structure/block-inject/%/delete'] = array(
    'title' => 'Delete block inject region',
    'page callback' => 'drupal_get_form',
//    'page arguments' => array('block_inject_adv_delete_confirm', 3),
    'access arguments' => array('administer block inject'),
    'type' => MENU_CALLBACK,
    'file' => 'block_inject_adv.admin.inc',
  );



  return $items;
}

/**
 * Checks if the node type exists already in the "block_inject_adv" table and
 * returns an array of the available node types (not used in any region).
 */
function block_inject_adv_get_node_types() {
  // Get all node types on the site.
  $site_node_types = block_inject_adv_get_all_node_types();

  // Get all the regions in the "block_inject_adv" table.
  $regions = block_inject_adv_get_regions();
  $existing_node_types = array();

  // Create array containing the node types used in regions.
  foreach ($regions as $region) {
    // Foreach region, check if it has multiple node types assigned.
    if (strpbrk($region->node_type, ', ') != FALSE) {
      // If multiple node types on a region, create array with the node types
      // for each of the regions with multiple node types.
      $existing_node_types_array = explode(', ', $region->node_type);
      // Add each of those node types into the final array.
      foreach ($existing_node_types_array as $type) {
        $existing_node_types[] = $type;
      }
    }
    else {
      // If only one node type in a region, add it to the final node type array.
      $existing_node_types[] = $region->node_type;
    }
  }
  // Update array and remove the node types that are used regions.
  foreach ($existing_node_types as $type) {
    //unset($site_node_types[$type]);
  }

  return $site_node_types;
}

/**
 * Implements hook_system_info_alter().
 */
function block_inject_adv_system_info_alter(&$info, $file, $type) {
  if ($type == 'theme') {
    // Using the deprecated function to retrieve the regions in order to avoid
    // problems during the update.php process.
    $regions = _block_inject_adv_get_regions_old();
    // Add regions to the site.
    foreach ($regions as $region) {
      $info['regions']['block_inject_adv-' . $region->id] = t('Block Inject: @title', array('@title' => $region->region));
    }
  }
}

/**
 * Implements hook_node_view().
 */
function block_inject_adv_node_view($node, $view_mode, $langcode) {

  // Only apply if we are looking at the full view mode.
  if ($view_mode !== 'full') {
    return;
  }

  // Check if there is an injectable region for this node type.
  $injectable = block_inject_adv_get_region_by_node_type($node->type);
  if (!$injectable) {
    return;
  }

  // Inject the region.
  $render = block_inject_adv_do_injection($node, $injectable);
  if ($render) {
    $node->content['body'][0]['#markup'] = $render;
  }

}

/**
 * Implements hook_panels_pane_content_alter().
 *
 * @param $content
 * @param $pane
 * @param $args
 * @param $contexts
 */
function block_inject_adv_panels_pane_content_alter($content, $pane, $args, $contexts) {
  $pane_types = array('entity_field', 'node_content');
  $pane_subtypes = array('node:body', 'node_content');
  if (!in_array($pane->type, $pane_types)) {
    return;
  }
  if (!in_array($pane->subtype, $pane_subtypes)) {
    return;
  }
  if (!isset($contexts['argument_entity_id:node_1'])) {
    return;
  }

  $node = $contexts['argument_entity_id:node_1']->data;

  // Check if there is an injectable region for this node type.
  $injectable = block_inject_adv_get_region_by_node_type($node->type);
  if (!$injectable) {
    return;
  }

  $render = block_inject_adv_do_injection($node, $injectable);
  if (!$render) {
    return;
  }

  // For the body field panel pane.
  if ($pane->subtype === 'node:body') {
    $content->content[0]['#markup'] = $render;
  }
  // For the node content panel pane.
  if ($pane->subtype === 'node_content') {
    $content->content['body'][0]['#markup'] = $render;
  }
}

/**
 * Helper function that prepares and calls the injection on a node.
 *
 * @param $node
 *  The node object that gets injected
 *
 * @param $injectable
 *  The injectable that goes into the node's body field
 *
 * @return string
 *  The markup of the injected body field.
 */
function block_inject_adv_do_injection($node, $injectable) {
  $offset = array();

  // Check for exceptions.
//  $exception = block_inject_adv_green_light($node->nid);
//  $go_ahead = FALSE;
//  if (empty($exception)) {
//    $go_ahead = TRUE;
//  }
//  elseif ($exception['bi_id'] != $injectable->id) { //@todo exception handling
//    $go_ahead = TRUE;
//  }
//
//  if (!$go_ahead) {
//    return FALSE;
//  }

  $stop =1;

  foreach ($injectable as $key => $injectable_data) {
    // Return the blocks for each region to be injected into a variable.
    $region[] = block_get_blocks_by_region('block_inject_adv-' . $injectable_data->id);

    // Prepare the attributes for the region div.
    $classes = array();
    $classes = array('block-inject', 'block-inject-' . $injectable_data->id);
    $attributes[] = array(
      'id' => 'block-inject-' . $injectable_data->id,
      'class' => $classes,
    );

    // Create hook_block_inject_adv_attributes_alter($attributes).
    $clearfix[] = TRUE;
    $vars[] = array(
      'node' => clone $node,
      'attributes' => &$attributes,
      'clearfix' => &$clearfix,
    );
    drupal_alter('block_inject_adv_attributes', $vars);

    // Get the body field.
    $body = block_inject_adv_get_body_from_node($node);

    // Check for default conditions for offset.
    $action = block_inject_adv_process_condition($injectable_data->id, $body);
    if ($action) {
      $action_data[] = $action;
    }

    //@todo form
    // Check if there is an individual offset on the node.
//    $get_offset = block_inject_adv_get_offset($node->nid);
//    if (!empty($get_offset)) {
//      $action_data[] = $offset + $get_offset['offset'];
//    }
  }


  // Try the injection and return the injected body.
  $stop=1;
  if ($render = block_inject_adv_node($region, $body, $action_data, $attributes, $clearfix)) {
    return $render;
  }
}

/**
 * Injects the region in the middle of the node.
 *
 * @param array $region
 *   An array of the regions to be injected.
 *
 * @param string $body
 *   The body field to be injected in.
 *
 * @param $action_data
 * @param array $attributes
 * @param bool $clearfix
 * @return bool|string The injected body string or false if not enough paragraphs.
 * The injected body string or false if not enough paragraphs.
 * @internal param int $positioning_data
 */
function block_inject_adv_node($region, $body, $action_data, $attributes = array(), $clearfix = TRUE) {

  // Break up the body field string into an array of paragraphs.
  $array_body_paragraphs = explode("<p>", $body);


  foreach ($region as $key => $data) {

    //3 actions avaliable, parapgraph_position, char_position, paragraph_offset
    $stop = 1;

    //parapgraph_position
    if (isset($action_data[$key]['parapgraph_position'])) {
      $region['#prefix'] = '<div ' . drupal_attributes($attributes[$key]) . '>';
      $region['#suffix'] = '</div>';
      // Add a clearfix div if requested
      if ($clearfix[$key] === TRUE) {
        $region['#suffix'] .= '<div class="clearfix"></div>';
      }

      //--Insert the renderable region array into paragraph position
      if (isset($action_data[$key]['parapgraph_position'])) {
        array_splice($array_body_paragraphs, ($action_data[$key]['parapgraph_position'])+1, 0, render($data));
      }

    }

    //char_position
    if (isset($action_data[$key]['char_position'])) {

      $region['#prefix'] = '<div ' . drupal_attributes($attributes[$key]) . '>';
      $region['#suffix'] = '</div>';
      // Add a clearfix div if requested
      if ($clearfix[$key] === TRUE) {
        $region['#suffix'] .= '<div class="clearfix"></div>';
      }

      //check char count and add after closest paragraph
      //for character insertion. we can not insert after like 300 characters
      // because that could lead to insertion in teh middle of a table or other weird places. so we use the closest paragraph
      //advanced solutioun must analyze code inside <p> ..
      $current_char = 0;
      $inserted = FALSE;

      foreach ($array_body_paragraphs as $body_key => $string) {
        $replace = array('\n', '\r', '\t');
        $array_body_chars[$body_key]['paragraph'] = str_replace($replace, '', trim(strip_tags($string)));
        $array_body_chars[$body_key]['char_count'] = mb_strlen($array_body_chars[$body_key]['paragraph']);
        $paragraph_char_pos = $body_key;
        $current_char = $current_char + $array_body_chars[$body_key]['char_count'];

        //insert after this paragraph
        if (($current_char >= $action_data[$key]['char_position']) && $inserted == FALSE) {
          //--Insert the renderable region array into paragraph position
          array_splice($array_body_paragraphs, $paragraph_char_pos + 1, 0, render($data));
          $inserted = TRUE;
        }
      }

    }


    //paragraph_offset
    if (isset($action_data[$key]['paragraph_offset'])) {
      // Get the half number of the total number of paragraphs (round up).
      $array_explode_half = round((count($array_body_paragraphs) - 1) / 2);
      // Add the attributes to the region renderable array
      $region['#prefix'] = '<div ' . drupal_attributes($attributes[$key]) . '>';
      $region['#suffix'] = '</div>';

      // Add a clearfix div if requested
      if ($clearfix[$key] === TRUE) {
        $region['#suffix'] .= '<div class="clearfix"></div>';
      }

      //--Insert the renderable region array into the middle of the body field.
      if (isset($action_data[$key]['paragraph_offset'])) {
        array_splice($array_body_paragraphs, ($array_explode_half + 1 + $action_data[$key]['paragraph_offset']), 0, render($data));
      }
    }
  }


  // Re-create the body field string with the regions injected.
  return implode("<p>", $array_body_paragraphs);
}


/**
 * Retrieves from the database the a region for a specific node type.
 *
 * @param $type
 *   The node type
 *
 * @return mixed
 *   The region or false if none exists
 */
function block_inject_adv_get_region_by_node_type($type) {
  $regions_array = array();

  $regions = block_inject_adv_get_regions();

  foreach ($regions as $region) {

    $node_types = explode(', ', $region->node_type);

    //multiple types
    if (in_array($type, $node_types)) {
      $regions_array[] = $region;
    }
    //single type
    else {
      if ($type == $region->node_type) {
        $regions_array[] = $region;
      }
    }
  }

  return $regions_array;

  //return FALSE;
}



/**
 * Implements hook_form_FORM-ID_alter().
 *
 * Adds to the node edit form the Block Inject elements (offset & exception)
 *
 * @param $form
 * @param $form_state
 * @param $form_id
 */
//function block_inject_adv_form_node_form_alter(&$form, &$form_state, $form_id) {
//  // Gets the node info in dedicated vars.
//  $node_type = $form['type']['#value'];
//  $node = $form['#node'];
//
//  // Stores the block inject ID for the node type of this form, if exists.
//  $bi = block_inject_adv_find_bi_id($node_type);
//
//  // Checks if $bi contains anything.
//  // If contains, can go ahead and add the exception checkbox onto the form.
//  if ($bi) {
//    // Checks if there is an exception in the table for this nid
//    $bi_exception_default_value = 0;
//    if (isset($form['nid']['#value'])) {
//      $check_exception = block_inject_adv_green_light($node->nid);
//      if ($check_exception) {
//        // If there is, sets the default value to 1, else it stays 0.
//        $bi_exception_default_value = 1;
//      }
//
//      // Set the default value for the offset select box.
//      $offset = block_inject_adv_get_offset($form['#node']->nid);
//      if (isset($offset['offset'])) {
//        $bi_offset_default_value = $offset['offset'];
//      }
//      else {
//        $bi_offset_default_value = 0;
//      }
//    }
//    else {
//      $bi_offset_default_value = 0;
//    }
//
//    // Get the body field.
//    $body = block_inject_adv_get_body_from_node($node);
//
//    // Check if the node form is on an already existing node.
//    if (isset($form['nid']['#value'])) {
//      // Get the number of paragraphs in the body.
//      $body_explode = explode("<p>", $body);
//      $paragraphs = (count($body_explode));
//      // Get the middle place in the body.
//      $middle = round($paragraphs / 2) - 1;
//
//      // Create the select field options.
//      $i = 0;
//      $half = array();
//      while ($i < $middle) {
//        $i++;
//        $half[] = $i;
//      }
//      if (count($half) >= 1) {
//        foreach (array_reverse($half) as $i) {
//          $offset_options[] = -$i;
//        }
//      }
//
//      $offset_options[] = 0;
//      if (count($half) >= 1) {
//        foreach ($half as $i) {
//          $offset_options[] = $i;
//        }
//      }
//    }
//    else {
//      $offset_options = array(0);
//    }
//
//    // Creates a new fieldset for the Block Inject settings.
//    $form['block_inject_adv'] = array(
//      '#type' => 'fieldset',
//      '#title' => t('Block Inject settings'),
//      '#collapsible' => TRUE,
//      '#collapsed' => TRUE,
//      '#group' => 'additional_settings',
//      '#weight' => $form['author']['#weight'] - 5,
//      '#attributes' => array(
//        'class' => array('block-inject-node-form'),
//      ),
//      '#attached' => array(
//        'js' => array(drupal_get_path('module', 'block_inject_adv') . '/block_inject_adv.js'),
//      ),
//      '#access' => user_access('override default block injection'),
//    );
//
//    // Check for existing condition on this node.
//    $region_id = block_inject_adv_find_bi_id($node->type);
//    $region = block_inject_adv_get_region_by_id($region_id->id);
//
//    $condition_offset = FALSE;
//    if ($region->bi_condition) {
//      $condition = unserialize($region->bi_condition);
//      $condition_offset = block_inject_adv_process_condition($node->type, $body);
//    }
//
//    // Add the markup field if there is a condition on the node.
//    if ($condition_offset) {
//      // Check if singular.
//      if ($condition_offset == '1' || $condition_offset == '-1') {
//        $condition_offset = $condition_offset . ' paragraph';
//      }
//      else {
//        $condition_offset = $condition_offset . ' paragraphs';
//      }
//      // Renders the markup field with the warning if there is a condition.
//      $form['block_inject_adv']['block_inject_adv_conditions_info'] = array(
//        '#type' => 'markup',
//        '#markup' => t('Attention, the injection in this node already is offset
//          by @number. Please take this into account if you want to offset more.', array('@number' => check_plain($condition_offset))),
//        '#prefix' => '<div class="messages warning">',
//        '#suffix' => '</div>'
//      );
//    }
//
//    // Renders the block inject exception checkbox.
//    $form['block_inject_adv']['block_inject_adv_exception'] = array(
//      '#type' => 'checkbox',
//      '#title' => t('Remove the Block Inject effect on this node'),
//      '#description' => t('This will prevent the region to be injected into this
//        node.'),
//      '#default_value' => $bi_exception_default_value,
//    );
//
//    // Renders the block inject offset select box
//    $form['block_inject_adv']['block_inject_adv_offset'] = array(
//      '#type' => 'select',
//      '#title' => t('Block Inject paragraph offset'),
//      '#description' => t('If the default injection place is not ideal within
//      the context of your node, you can move it up or down by providing an
//      offset of the number of paragraphs you would like it moved by.'),
//      '#options' => array_combine($offset_options, $offset_options),
//      '#default_value' => $bi_offset_default_value,
//    );
//  }
//}

/**
 * Implements hook_node_submit().
 *
 * Saves to the block_inject_adv_exceptions table the exception.
 */
//function block_inject_adv_node_submit($node, $form, &$form_state) {
//  $nid = $form_state['values']['nid'];
//  $node_type = $form_state['values']['type'];
//  $bi = block_inject_adv_find_bi_id($node_type);
//  if (isset($form_state['values']['block_inject_adv_offset'])) {
//    $offset = $form_state['values']['block_inject_adv_offset'];
//  }
//  else {
//    $offset = 0;
//  }
//
//  if (isset($form_state['values']['block_inject_adv_exception'])) {
//    if ($form_state['values']['block_inject_adv_exception'] == 1) {
//      if ($offset != 0) {
//        block_inject_adv_insert_exception($nid, $bi->id, 1, $offset);
//      }
//      else {
//        block_inject_adv_insert_exception($nid, $bi->id, 1);
//      }
//    }
//    if ($form_state['values']['block_inject_adv_exception'] == 0) {
//      if ($offset == 0) {
//        block_inject_adv_remove_exception('nid', $nid);
//      }
//      else {
//        block_inject_adv_insert_exception($nid, $bi->id, 0, $offset);
//      }
//    }
//  }
//  elseif ($offset != 0) {
//    block_inject_adv_insert_exception($nid, $bi->id, 0, $offset);
//  }
//}

/**
 * Helper function to process the condition for an injection.
 *
 * @param $id
 * @param $body
 *   The node body
 *
 * @return bool|array False if no condition, or the action data
 * @internal param $node_type The node type*   The node type
 *
 */
function block_inject_adv_process_condition($id, $body) {
  // Get injectable region ID.
  //$id = block_inject_adv_find_bi_id($id);
  // Get the injectable region.
  $region = block_inject_adv_get_region_by_id($id);

  if ($region->bi_condition) {

    // Get condition information.
    $data = unserialize($region->bi_condition);

    //PARAGRAPHS
    // Break up the body field string into an array of paragraphs and count them.
    $array_explode = explode("<p>", render($body));
    $body_paragraphs_count = count($array_explode);

    //CHARACTERS
    $body_html_stripped = strip_tags(render($body));
    $body_char_count = count($body_html_stripped);

    $paragraph_operator = $data['condition']['paragraph_operator'];
    $char_operator = $data['condition']['char_operator'];
    $paragraph_no = $data['condition']['paragraph_no'];
    $char_no = $data['condition']['char_no'];
    $condition_boole = $data['condition']['condition_boole'];

    //3 action currently possible, see settings
    $action_data = $data['action'];

    // init states
    $paragraph_state = FALSE;
    $char_state = FALSE;


    //@todo this state handling could be made more generic for more cases or data
    //-- AND
    if ($condition_boole == 'and') {

      //1 paragraph AND character
      if (isset($paragraph_no) && isset($char_no)) {
        $paragraph_state = block_inject_adv_operator_check($paragraph_operator, $paragraph_no, $body_paragraphs_count);
        $char_state = block_inject_adv_operator_check($char_operator, $char_no, $body_char_count);
        if ($paragraph_state && $char_state) {
          $inject_state = TRUE;
        }
      }
      // only paragraph
      if (isset($paragraph_no)) {
        $paragraph_state = block_inject_adv_operator_check($paragraph_operator, $paragraph_no, $body_paragraphs_count);
        if ($paragraph_state) {
          $inject_state = TRUE;
        }
      }
      //only char
      if (isset($char_no)) {
        $char_state = block_inject_adv_operator_check($char_operator, $char_no, $body_char_count);
        if ($char_state) {
          $inject_state = TRUE;
        }
      }

    }

    //-- OR
    if ($condition_boole == 'or') {

      //2 paragraph OR character
      if (isset($paragraph_no) || isset($char_no)) {
        $paragraph_state = block_inject_adv_operator_check($paragraph_operator, $paragraph_no, $body_paragraphs_count);
        $char_state = block_inject_adv_operator_check($char_operator, $char_no, $body_char_count);
        if ($paragraph_state && $char_state) {
          $inject_state = TRUE;
        }
      }
      // only paragraph
      if (isset($paragraph_no)) {
        $paragraph_state = block_inject_adv_operator_check($paragraph_operator, $paragraph_no, $body_paragraphs_count);
        if ($paragraph_state) {
          $inject_state = TRUE;
        }
      }
      //only char
      if (isset($char_no)) {
        $char_state = block_inject_adv_operator_check($char_operator, $char_no, $body_char_count);
        if ($char_state) {
          $inject_state = TRUE;
        }
      }

    }

  }

  //return action or false if no action will be used (no injection)
  if ($inject_state) {
    return $action_data;
  }
  else {
    return FALSE;
  }

}


/**
 * checking operator, user condition aganinst actual numer of chars/paragraphs of body field
 *
 * @param $operator
 *    operator can be: >, <, =
 * @param $compare_to
 *    value to compare. e.g the number of paragraphs or chars in the body
 * @param $uservalue
 *    value to compare too, input from user settingsform, e.g 5 (parapgraphs) or 300 (chars)
 *
 * @return bool
 */
function block_inject_adv_operator_check($operator, $uservalue, $compare_to) {

  $stop = 1;

  // Process condition.
  switch ($operator) {
    case '=':
      if ($compare_to = $uservalue) {
        return TRUE;
      }
      break;
    case '>':
      if ($compare_to > $uservalue) {
        return TRUE;
      }
      break;
    case '<':
      if ($compare_to < $uservalue) {
        return TRUE;
      }
      break;
  }

  return FALSE;

}

/**
 * Helper function to retrieve proper body value from the node.
 *
 * @param object $node
 *   The node object of the body.
 *
 * @return string
 *   Sanitized body value or false if problems.
 */

function block_inject_adv_get_body_from_node($node) {

  $body = field_get_items('node', $node, 'body');

  if (isset($body[0]['safe_value'])) {
    return $body[0]['safe_value'];
  }

  if ($body[0]['format'] === 'plain_text') {
    return check_plain($body[0]['value']);
  }
  else {
    return check_markup($body[0]['value'], $body[0]['format']);
  }
}


/**
 * Retrieves the ID of the block_inject_adv rule based on node type provided.
 *
 * Returns an object containing property id.
 * @param $node_type
 * @return
 */
function block_inject_adv_find_bi_id($node_type) {
  $result = db_select('block_inject_adv', 'bi')
    ->fields('bi', array('id'))
    ->condition('node_type', '%' . $node_type . '%', 'LIKE')
    ->execute()
    ->fetch();
  return $result;
}

/**
 * Inserts the exception into the database
 *
 * @param      $nid
 * @param      $bi_id
 * @param      $exception
 * @param null $offset
 *
 * @throws \Exception
 */
//function block_inject_adv_insert_exception($nid, $bi_id, $exception, $offset = NULL) {
//  $existing = db_select('block_inject_adv_exceptions', 'bie')
//    ->fields('bie', array('id'))
//    ->condition('nid', $nid)
//    ->execute()
//    ->fetch();
//  if ($existing !== FALSE) {
//    $query = db_update('block_inject_adv_exceptions')
//      ->fields(array(
//        'except_injection' => $exception,
//        'offset' => $offset,
//      ))
//      ->condition('nid', $nid)
//      ->execute();
//  }
//  else {
//    $insert = db_insert('block_inject_adv_exceptions')
//      ->fields(array(
//        'bi_id' => $bi_id,
//        'nid' => $nid,
//        'except_injection' => $exception,
//        'offset' => $offset,
//      ))
//      ->execute();
//  }
//}

/**
 * Removes the exception from the database
 *
 * @param $type
 * @param $selector
 */
//function block_inject_adv_remove_exception($type, $selector) {
//  $db_remove = db_delete('block_inject_adv_exceptions');
//  if ($type == 'nid') {
//    $db_remove->condition('nid', $selector);
//  }
//  if ($type == 'bi_id') {
//    $db_remove->condition('bi_id', $selector);
//  }
//  $db_remove->execute();
//}

/**
 * Checks to see if there is an exception in the block_inject_adv_exceptions table
 *
 * @param int $nid
 *   The node ID of the node to check for
 *
 * Returns an array with the ID of the block_inject_adv rule
 */
//function block_inject_adv_green_light($nid) {
//  $result = db_select('block_inject_adv_exceptions', 'bie')
//    ->fields('bie', array('bi_id', 'except_injection'))
//    ->condition('nid', $nid)
//    ->condition('except_injection', 1)
//    ->execute()
//    ->fetchAssoc();
//  return $result;
//}

/**
 * Checks to see if there is an offset for a particular node
 *
 * @param int $nid
 *   The node ID of the node to check for
 *
 * Returns the offset
 */
//function block_inject_adv_get_offset($nid) {
//  $result = db_select('block_inject_adv_exceptions', 'bie')
//    ->fields('bie', array('offset'))
//    ->condition('nid', $nid)
//    ->isNotNull('offset')
//    ->execute()
//    ->fetchAssoc();
//  return $result;
//}

/**
 * Retrieves from the database the existing exceptions from injection.
 *
 * Returns an array of node objects ordered by node ID
 */
//function block_inject_adv_get_exceptions() {
//  $select = db_select('block_inject_adv_exceptions', 'bie')
//    ->fields('bie', array('nid', 'bi_id'))
//    ->orderBy('nid', 'ASC')
//    ->execute();
//
//  $nids_object = $select->fetchAll();
//
//  return $nids_object;
//}

/**
 * Returns all the content types available on the site.
 */
function block_inject_adv_get_all_node_types() {
  $query = db_select('node_type', 'nt')
    ->fields('nt', array('type', 'name'));
  $result = $query->execute();
  $records = array();
  foreach ($result as $record) {
    $records[$record->type] = check_plain($record->name);
  }
  return $records;
}

/**
 * Retrieves one existing declared region for injection.
 *
 * @param $id
 *
 * @return
 */
function block_inject_adv_get_region_by_id($id) {
  $result = db_select('block_inject_adv', 'bi')
    ->fields('bi', array(
      'region',
      'node_type',
      'id',
      'node_name',
      'bi_condition'
    ))
    ->condition('id', $id)
    ->orderBy('region', 'ASC')
    ->execute()
    ->fetchObject();

  return $result;
}

/**
 * Retrieves from the database the existing declared regions for injection.
 *
 * Deprecated function introduced for the smooth update to 7.x-1.2-alpha4
 */
function _block_inject_adv_get_regions_old() {
  $result = db_select('block_inject_adv', 'bi')
    ->fields('bi', array('region', 'node_type', 'id', 'node_name'))
    ->orderBy('region', 'ASC')
    ->execute();

  return $result;
}

/**
 * Retrieves from the database the existing declared regions for injection.
 */
function block_inject_adv_get_regions() {
  $result = db_select('block_inject_adv', 'bi')
    ->fields('bi', array(
      'region',
      'node_type',
      'id',
      'node_name',
      'bi_condition'
    ))
    ->orderBy('region', 'ASC')
    ->execute();

  return $result;
}

/**
 * Rebuilds data, clears caches.
 */
function block_inject_adv_rebuild_data() {
  cache_clear_all();
  system_rebuild_theme_data();
  drupal_theme_rebuild();
}

/**
 * Adds a new region to the database
 *
 * @param string $region
 *   The region name.
 *
 * @param string $node_type
 *   The machine readable node type name it applies for.
 *
 * @param string $node_type_name
 *   The human readable node type name it applies for.
 *
 * @param string $condition
 *   Serialized array for the condition.
 */
function block_inject_adv_add_region($region, $node_type, $node_type_name, $condition = NULL) {
  $insert = db_insert('block_inject_adv')
    ->fields(array(
      'region' => $region,
      'node_type' => $node_type,
      'node_name' => $node_type_name,
      'bi_condition' => $condition,
    ))
    ->execute();
}

/**
 * Deletes a region from the database.
 *
 * @param int $id
 *   Id of the region to be deleted.
 */
function block_inject_adv_remove_region($id) {
  $db_remove = db_delete('block_inject_adv')
    ->condition('id', $id)
    ->execute();
  block_inject_adv_rebuild_data();
}

/**
 * Updates a region in the database.
 *
 * @param int $id
 *   Id of the region to be updated.
 *
 * @param string $new_region
 *   The new region name.
 *
 * @param string $new_node_type
 *   The new machine readable node type name it applies for.
 *
 * @param string $new_node_type_name
 *   The new human readable node type name it applies for.
 * @param null $condition
 */
function block_inject_adv_update_region($id, $new_region, $new_node_type, $new_node_type_name,
                                    $condition = NULL) {
  $db_edit = db_update('block_inject_adv')
    ->fields(array(
      'region' => $new_region,
      'node_type' => $new_node_type,
      'node_name' => $new_node_type_name,
      'bi_condition' => $condition,
    ))
    ->condition('id', $id)
    ->execute();
}

/**
 * Checks for duplicate region names.
 *
 * @param string $region_name
 *   Name of the region to be checked.
 *
 * @return FALSE if the region name is already used, TRUE if it is not.
 */
function block_inject_adv_check_region_name($region_name) {
  $result = db_select('block_inject_adv', 'bi')
    ->fields('bi', array('region'))
    ->condition('region', $region_name)
    ->orderBy('region', 'ASC')
    ->execute()
    ->fetchAssoc();
  if (strtolower($result['region']) == strtolower($region_name)) {
    return FALSE;
  }
  else {
    return TRUE;
  }
}



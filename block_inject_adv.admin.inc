<?php

/**
 * @file
 * The admin functions for the module.
 *
 * Handles the administration interface callbacks.
 */

/**
 * Callback for the main Block Inject page.
 */
function block_inject_adv_list() {
  $result = block_inject_adv_get_regions();
  $header = array(
    array('data' => t('Inject Regions')),
    array('data' => t('Condition by default')),
    array('data' => t('Operations'), 'colspan' => 2),
  );
  $rows = array();
  foreach ($result as $row) {
    // Check if there is condition.
    if ($row->bi_condition) {
      $condition = unserialize($row->bi_condition);
    }
    $tablerow = array(
      array('data' => check_plain($row->region) . ' | ' . check_plain($row->node_name)),
      array('data' => isset($condition) ? 'Yes' : 'No'),
      array('data' => l(t('Edit'), 'admin/structure/block-inject/' . $row->id . '/edit')),
      array('data' => l(t('Delete'), 'admin/structure/block-inject/' . $row->id . '/delete')),
    );
    $rows[] = $tablerow;
  }
  if (!$rows) {
    $rows[] = array(
      array(
        'data' => t('No inject regions available.'),
        'colspan' => 4
      )
    );
  }
  $build = array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $rows,
    '#attributes' => array('id' => 'block_inject_adv'),
  );

  return $build;
}

/**
 * Callback for the Block Inject Exceptions page.
 */
function block_inject_adv_exceptions_list() {
  $result = block_inject_adv_get_exceptions();
  $header = array(
    array('data' => t('Excepted Nodes')),
    array('data' => t('Node type')),
    array('data' => t('Type of exception')),
    array('data' => t('Offset')),
    array('data' => t('Offset from default condition')),
    array('data' => t('Edit')),
  );
  $rows = array();
  if (isset($result)) {
    foreach ($result as $row) {
      $node = node_load($row->nid);
      $node_type = node_type_load($node->type);

      // Check the exception value for the node.
      $get_exception = block_inject_adv_green_light($row->nid);
      if ($get_exception['except_injection'] == 1) {
        $exception = 'Not injected';
      }
      else {
        $exception = 'Injected';
      }

      // Check the offset for the node.
      $get_offset = block_inject_adv_get_offset($row->nid);
      if ($get_offset != 0) {
        $offset = $get_offset['offset'] . ' paragraphs';
      }
      else {
        $offset = 'None';
      }

      // Check for existing condition on this node.
      $region_id = block_inject_adv_find_bi_id($node->type);
      $region = block_inject_adv_get_region_by_id($region_id->id);
      if ($region->bi_condition) {
        $condition = unserialize($region->bi_condition);
        // Get the body field.
        $body_field = field_get_items('node', $node, 'body');
        $body = $body_field[0]['safe_value'];
        $condition_offset = block_inject_adv_process_condition($node->type, $body);
      }

      $tablerow = array(
        array('data' => l(check_plain($node->title), 'node/' . $node->nid)),
        array('data' => check_plain($node_type->name)),
        array('data' => $exception),
        array('data' => $offset),
        array('data' => isset($condition_offset) ? $condition_offset . ' paragraphs' : 'No'),
        array('data' => l(t('Edit'), 'node/' . $node->nid . '/edit')),
      );
      $rows[] = $tablerow;
    }
  }

  if (!$rows) {
    $rows[] = array(
      array(
        'data' => t('No exceptions available.'),
        'colspan' => 6
      )
    );
  }
  $build = array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $rows,
    '#attributes' => array('id' => 'block_inject_adv_exceptions'),
  );

  return $build;
}

/**
 * Callback form to add new region.
 *
 * @param $form
 * @param $form_state
 *
 * @param null $id
 *    id is populated in edit mode otherwise it is add
 * @return
 */
function block_inject_adv_add_inject_form($form, &$form_state, $id = NULL) {

  $stop = 1;

  //we are in edit mode, load existing data
  if (!is_null($id)) {
    // Get the region by the ID that came through the the URL.
    $region = block_inject_adv_get_region_by_id($id);

    // Pass the region id through the form_state for the submit function.
    $form_state['block_inject_adv_region_id'] = $id;
    $form['block_inject_adv_region_name'] = array(
      '#type' => 'textfield',
      '#title' => t('Region name'),
      '#description' => t('What is the name of the region you would like to inject?'),
      '#required' => TRUE,
      '#default_value' => check_plain($region->region),
    );

    // Get the available node types to be selected.
    $available_node_types = block_inject_adv_get_node_types();

    // Get the node types selected by this region.
    $sel_node_type_machine_array = explode(', ', $region->node_type);
    $sel_node_type_name_array = explode(', ', $region->node_name);
    $selected_node_types = array_combine($sel_node_type_machine_array, $sel_node_type_name_array);

    // Add to the available node types the ones selected by this region.
    $node_types_options = array_merge($available_node_types, $selected_node_types);

    // Unserialize condition.
    if ($region->bi_condition) {
      $condition = unserialize($region->bi_condition);
    }
    else {
      drupal_set_message('Fatal erro gettingsettings of block inject', 'error');
    }
  }

  $STOP=1;

  $form['block_inject_adv_region_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Region name'),
    '#description' => t('What is the name of the region you would like to inject?'),
    '#required' => TRUE,
    '#default_value' => isset($region->region) ? check_plain($region->region) : '',
  );

  // Return available node types to have as options for the select form element.
  $node_types = block_inject_adv_get_node_types();
  $form['block_inject_adv_content_type'] = array(
    '#type' => 'select',
    '#title' => t('Available Content Type(s)'),
    '#description' => t('Which content type would you like this region to be
      injected'),
    '#options' => $node_types,
    '#multiple' => TRUE,
    '#default_value' => isset($selected_node_types) ? array_flip($selected_node_types) : '',
  );


  //conditional palcement option, wrapping fieldset
  $form['block_inject_adv_conditionals_toggle'] = array(
    '#title' => t('Set a condition for the placement of the region injection?'),
    '#type' => 'checkbox',
    //set default to activate because using this really makes sense
    '#default_value' => $region->bi_condition ? 1 : 0,
  );
  $form['block_inject_adv_conditionals'] = array(
    '#title' => t('The condition when the block is placed'),
    '#type' => 'fieldset',
    // Show only if conditional checkbox is selected.
    '#states' => array(
      'visible' => array(
        'input[name="block_inject_adv_conditionals_toggle"]' => array('checked' => TRUE),
      ),
    )
  );

  //number of paragraphs
  $form['block_inject_adv_conditionals']['pg_cond'] = array(
    '#title' => t('Paragraphs'),
    '#type' => 'fieldset',
    '#prefix' => '<div class="container-inline">',
    '#suffix' => '</div>',
    // Show only if conditional checkbox is selected.
    '#states' => array(
      'visible' => array(
        'input[name="block_inject_adv_conditionals_toggle"]' => array('checked' => TRUE),
      ),
    )
  );
  $form['block_inject_adv_conditionals']['pg_cond']['block_inject_adv_explanation_1'] = array(
    '#type' => 'markup',
    '#markup' => t('The Body Text needs to have ')
  );
  $form['block_inject_adv_conditionals']['pg_cond']['block_inject_adv_paragraph_operator'] = array(
    '#type' => 'select',
    '#options' => array(
      '<' => 'less than',
      '=' => 'exactly',
      '>' => 'more than'
    ),
    '#multiple' => FALSE,
    '#default_value' => isset($condition) ? $condition['condition']['paragraph_operator'] : '>',
  );
  $form['block_inject_adv_conditionals']['pg_cond']['block_inject_adv_paragraph_number'] = array(
    '#type' => 'textfield',
    '#maxlength' => 3,
    '#size' => 4,
    '#default_value' => isset($condition) ? $condition['condition']['paragraph_no'] : '1',
  );
  $form['block_inject_adv_conditionals']['pg_cond']['block_inject_adv_explanation'] = array(
    '#type' => 'markup',
    '#markup' => t('paragraphs in order to fire one of the the actions below.')
  );

  //and/or
  $form['block_inject_adv_conditionals']['and_cond'] = array(
    '#title' => t(''),
    '#type' => 'fieldset',
    '#prefix' => '<div class="container-inline">',
    '#suffix' => '</div>',
    // Show only if conditional checkbox is selected.
    '#states' => array(
      'visible' => array(
        'input[name="block_inject_adv_conditionals_toggle"]' => array('checked' => TRUE),
      ),
    )
  );

  //AND/OR
  $form['block_inject_adv_conditionals']['and_cond'] ['and_or'] = array(
    '#type' => 'select',
    '#title' => t('AND/OR?'),
    '#options' => array(
      'and' => t('AND'),
      'or' => t('OR'),
    ),
    '#default_value' => isset($condition) ? $condition['condition']['condition_boole'] : 'and',
    '#description' => t('Set this to <em>AND</em> if you would like both to apply. Set it to OR if only one has to be valid for the insert to happen.'),
  );

  $form['block_inject_adv_conditionals']['char_cond'] = array(
    '#title' => t('Characters'),
    '#type' => 'fieldset',
    '#prefix' => '<div class="container-inline">',
    '#suffix' => '</div>',
    // Show only if conditional checkbox is selected.
    '#states' => array(
      'visible' => array(
        'input[name="block_inject_adv_conditionals_toggle"]' => array('checked' => TRUE),
      ),
    )
  );

  //character condition
  $form['block_inject_adv_conditionals']['char_cond']['block_inject_adv_explanation_char'] = array(
    '#type' => 'markup',
    '#markup' => t('The Body Text needs to have ')
  );
  $form['block_inject_adv_conditionals']['char_cond']['block_inject_adv_char_operator'] = array(
    '#type' => 'select',
    '#options' => array(
      '<' => 'less than',
      '=' => 'exactly',
      '>' => 'more than'
    ),
    '#multiple' => FALSE,
    '#default_value' => isset($condition) ? $condition['condition']['char_operator'] : '>',
  );
  $form['block_inject_adv_conditionals']['char_cond']['block_inject_adv_char_number'] = array(
    '#type' => 'textfield',
    '#default_value' => isset($condition) ? $condition['condition']['char_no'] : '1',
    '#maxlength' => 3,
    '#size' => 4,
  );
  $form['block_inject_adv_conditionals']['char_cond']['block_inject_adv_explanation_char_1'] = array(
    '#type' => 'markup',
    '#markup' => t('charcters in order to fire one of the the actions below.')
  );


  //info
  $form['block_inject_adv_insert_information'] = array(
    '#markup' => t('<h2>Block placement Settings</h2><p><b>Only use one condition above, do not use multiple. If a field is left empty it ist not used/disabled.</b></p>'),
  );


  //condition 1, middle injection
  //injekt paragraph

  $form['block_inject_adv_paragraph_offset_action'] = array(
    '#title' => t('Paragraph Offset Placement from the Middle of the Body Text'),
    '#type' => 'fieldset',
    '#description' => t('Insert the block by applying the above offset value. The offset will be added to the middle
    position. If you chose 0 the block is inserted in the middle of the body text.'),
  );
  $form['block_inject_adv_paragraph_offset_action']['block_inject_adv_paragraph_offset'] = array(
    '#title' => t('Paragraph offset'),
    '#type' => 'textfield',
    '#default_value' => isset($condition) ? $condition['action']['paragraph_offset'] : '',
    '#description' => t('Please specify a positive or negative number to
      offset the injection (e.g. 1 to move down by one paragraph /
      -1 to move up by one paragraph).'),
  );

  //condition 2, injection after paragraph, length
  //
  $form['block_inject_adv_paragraph_action'] = array(
    '#title' => t('Insert after Paragraph ...'),
    '#type' => 'fieldset',
    '#description' => t('Insert the block after a number of paragraphs. Insert the Number above.'),
  );
  $form['block_inject_adv_paragraph_action']['block_inject_adv_paragraph_position'] = array(
    '#title' => t('Insert Paragraph'),
    '#type' => 'textfield',
    '#default_value' => isset($condition) ? $condition['action']['parapgraph_position'] : '',
    '#description' => t('Please specify a positive number to the injection (e.g. 1, 2, 3, 4). If that node doesnt
    have that amount of paragraphs. Nothing will be injected.'),
  );


  //condition 3, after amount of characters
  //
  $form['block_inject_adv_char_action'] = array(
    '#title' => t('Insert after amount of characters ...'),
    '#type' => 'fieldset',
    '#description' => t('Insert the block after a number of charcters. Insert the Number above.'),
  );
  $form['block_inject_adv_char_action']['block_inject_adv_char_position'] = array(
    '#title' => t('Insert Characters'),
    '#type' => 'textfield',
    '#default_value' => isset($condition) ? $condition['action']['char_position'] : '',
    '#description' => t('Please specify a positive number to the injection (e.g. 1, 2, 3, 4). If that node doesnt
      have that amount of characters. Nothing will be injected.'),
  );


  //submit
  $form['block_inject_adv_region_submit'] = array(
    '#type' => 'submit',
    '#value' => t('Create region'),
  );

  return $form;
}

/**
 * Validation function for the Block Inject add/edit form.
 */
function block_inject_adv_add_inject_form_validate($form, &$form_state) {
  // Check for duplicate name.
  $region_name = $form_state['values']['block_inject_adv_region_name'];
  $test = block_inject_adv_check_region_name($region_name);

  //@todo
  //validation
}

/**
 * Submit form that adds the new region to the database.
 *
 * @param $form
 * @param $form_state
 */
function block_inject_adv_add_inject_form_submit($form, &$form_state) {
  $region = $form_state['values']['block_inject_adv_region_name'];

  //we are updating
  if(isset($form_state['block_inject_adv_region_id'])) {
    $update = TRUE;
  }
  else {
    //new isnert
    $update = FALSE;
  }

  if (isset($form_state['values']['block_inject_adv_content_type'])) {
    // If set, retrieve the machine name of the selected node typs.
    $node_type_array = $form_state['values']['block_inject_adv_content_type'];
    // Retrieve the names of the selected node types.
    $node_type_name_array = array_flip(array_intersect(array_flip($form_state['complete form']['block_inject_adv_content_type']['#options']), $node_type_array));
    // Implode the node type arrays.
    $node_type_name = check_plain(implode(', ', $node_type_name_array));
    $node_type = check_plain(implode(', ', $node_type_array));
  }
  else {
    $node_type = '';
    $node_type_name = '';
  }

  // Check if region name was provided, process condition and add to database.
  //only one action allowed in combinations with the conditions AND/OR
  $action_set = FALSE;

  if ($region) {

    // Process conditions AND/OR
    // --
    if ($form_state['values']['block_inject_adv_conditionals_toggle'] == 1) {
      if (is_numeric($form_state['values']['block_inject_adv_paragraph_number']) || is_numeric($form_state['values']['block_inject_adv_char_number'])) {
        $condition = array(
          'condition' => array(
            'paragraph_operator' => $form_state['values']['block_inject_adv_paragraph_operator'],
            'paragraph_no' => $form_state['values']['block_inject_adv_paragraph_number'],
            'char_operator' => $form_state['values']['block_inject_adv_char_operator'],
            'char_no' => $form_state['values']['block_inject_adv_char_number'],
            'condition_boole' => $form_state['values']['and_or'],
          ),
        );
      }
      else {
        // Need to do a better validation handling here with _validate.
        return drupal_set_message(t('Please make sure you have set at leaset one condition and it must be an integer'), 'error');
      }
    }

    //Process conditions 1 for the inject if there are any
    // --
    if (is_numeric($form_state['values']['block_inject_adv_paragraph_offset']) && $action_set == FALSE) {
      $condition['action'] = array(
        'paragraph_offset' => $form_state['values']['block_inject_adv_paragraph_offset'],
      );
      $action_set = TRUE;
    }
    else {
      if (!empty($form_state['values']['block_inject_adv_paragraph_offset'])) {
        // Need to do a better validation handling here with _validate.
        return drupal_set_message(t('Please make sure the paragraph offset number is an
            integer'), 'error');
      }
    }

    // Process conditions 2 for the inject if there are any
    // --
    if (is_numeric($form_state['values']['block_inject_adv_paragraph_position']) && $action_set == FALSE) {
      $condition['action'] = array(
        'parapgraph_position' => $form_state['values']['block_inject_adv_paragraph_position'],
      );
      $action_set = TRUE;
    }
    else {
      if (!empty($form_state['values']['block_inject_adv_paragraph_position'])) {
        // Need to do a better validation handling here with _validate.
        return drupal_set_message(t('Please make sure the paragraph position number is an
            integer'), 'error');
      }
    }

    // Process conditions 3 for the inject if there are any
    // --
    if (is_numeric($form_state['values']['block_inject_adv_char_position']) && $action_set == FALSE) {
      $condition['action'] = array(
        'char_position' => $form_state['values']['block_inject_adv_char_position'],
      );
      $action_set = TRUE;
    }
    else {
      if (!empty($form_state['values']['block_inject_adv_char_position'])) {
        // Need to do a better validation handling here with _validate.
        return drupal_set_message(t('Please make sure the chracter number is an
            integer'), 'error');
      }
    }

    //now serialize the data and save to db
    $serialized = serialize($condition);

    //update or new insert
    if($update == TRUE) {
      $id = $form_state['block_inject_adv_region_id'];
      block_inject_adv_update_region($id, $region, $node_type, $node_type_name, $serialized);
    }
    else {
      block_inject_adv_add_region($region, $node_type, $node_type_name, $serialized);
    }


//    else {
//            block_inject_adv_add_region($region, $node_type, $node_type_name);
//        }


    // Redirect back to main Block Inject page.
    $form_state['redirect'] = 'admin/structure/block-inject';

    // Rebuild data and clear cache.
    block_inject_adv_rebuild_data();
    drupal_set_message(t('Your region has been added.'), 'status');
  }
  else {
    drupal_set_message(t('There was an error'), 'error');
  }
}



/**
 * Submit form to edit a region.
 *
 * @param $form
 * @param $form_state
 */
function block_inject_adv_edit_form_submit($form, &$form_state) {
  // Get region
  $region = $form_state['values']['block_inject_adv_region_name'];
  // Run the name through the function to check if it is already in use.
  //$name_test = block_inject_adv_check_region_name($region);

//  if (($name_test == TRUE) || ($region == $form['block_inject_adv_region_name']['#default_value'])) {
//    if (isset($form_state['input']['block_inject_adv_content_type'])) {
//      // If set, retrieve the machine name of the selected node typs.
//      $node_type_array = $form_state['input']['block_inject_adv_content_type'];
//      // Retrieve the names of the selected node types.
//      $node_type_name_array = array_flip(array_intersect(array_flip($form_state['complete form']['block_inject_adv_content_type']['#options']), $node_type_array));
//      // Implode the node type arrays.
//      $node_type_name = check_plain(implode(', ', $node_type_name_array));
//      $node_type = check_plain(implode(', ', $node_type_array));
//    }
//    else {
//      $node_type = '';
//      $node_type_name = '';
//    }
//    // Check if region name was provided and add to database.
//    if ($region) {
//      $id = $form_state['block_inject_adv_region_id'];
//      // Process conditions for the inject if there are any
//      if ($form_state['values']['block_inject_adv_conditionals_toggle'] == 1) {
//        if (is_numeric($form_state['values']['block_inject_adv_paragraph_number']) &&
//          is_numeric($form_state['values']['block_inject_adv_paragraph_offset'])
//        ) {
//          $condition = array(
//            'condition' => array(
//              'operator' => $form_state['values']['block_inject_adv_paragraph_operator'],
//              'paragraph_no' => $form_state['values']['block_inject_adv_paragraph_number'],
//            ),
//            'action' => array(
//              'offset' => $form_state['values']['block_inject_adv_paragraph_offset'],
//            ),
//          );
//        }
//        $serialized = serialize($condition);
//        block_inject_adv_update_region($id, $region, $node_type, $node_type_name, $serialized);
//      }
//      else {
//        block_inject_adv_update_region($id, $region, $node_type, $node_type_name);
//      }
//      // Redirect back to main Block Inject page.
//      $form_state['redirect'] = 'admin/structure/block-inject';
//      block_inject_adv_rebuild_data();
//      drupal_set_message(t('Your region has been updated.'), 'status');
//    }
//    else {
//      drupal_set_message(t('There was an error'), 'error');
//    }
//  }
//  else {
//    drupal_set_message(t('Duplicate region name. Please choose another one!'), 'error');
//  }
}

/**
 * Callback function for confirming the inject region delete.
 *
 * @param     $form
 * @param     $form_state
 * @param int $region_id
 *   Id of the region to be deleted.
 *
 * @return
 */
function block_inject_adv_delete_confirm($form, &$form_state, $region_id) {
  $region = block_inject_adv_get_region_by_id($region_id);
  // Pass the region through the form_state for the submit function.
  $form_state['block_inject_adv_region'] = $region;
  return confirm_form($form, t('Are you sure you want to delete the block inject region %title ?', array('%title' => $region->region)), 'admin/structure/block-inject', 'The blocks that are assigned to this region will be reassigned. This action cannot be undone!', t('Delete'), t('Cancel'));
}

/**
 * Callback function to delete the inject region from the database.
 *
 * @param $form
 * @param $form_state
 */
function block_inject_adv_delete_confirm_submit($form, &$form_state) {
  $region = $form_state['block_inject_adv_region'];
  block_inject_adv_remove_region($region->id);
  //block_inject_adv_remove_exception('bi_id', $region->id); conditions for singel nodes removed
  drupal_set_message(t('The region %title has been removed.', array('%title' => $region->region)));
  $form_state['redirect'] = 'admin/structure/block-inject';
}
